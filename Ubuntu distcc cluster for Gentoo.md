# Ubuntu distcc cluster for Gentoo

You are probably wondering where this is heading and if you thought that it has something to do with distributed compilation then you are correct!

### Summary
In short this project will be about setting up your very own cluster to help Gentoo out with compiling its software faster.
This setup will provide you with the following options and features:
Restrict access to the distcc server via SSH or UFW(Firewall)
Allow different compiler versions/configurations to be used
Have a stable system that is quick to update to perform the compilation

### Required knowledge
To be able to understand what is going on in this guide you would need to have at least basic knowledge of the following subjects/softwares:
 - UFW(Uncomplicated firewall)
 - LXC/LXD(Container virtualization software utilizing the kernel of the hostmachine)
 - distcc(A daemon that allows remote distributed compilation of software)
 - Gentoo{Portage}(A high performance source based Linux distribution that can be highly tailored towards the end users software and hardware needs)

### Why Gentoo and why do I need this?

Gentoo relies on the Portage package manager for all its software packages.
These packages are called ebuilds, these ebuilds contain information about how the package should be compiled by the compilation software available on the system.
Gentoo utilizes useflags to give the user the option to remove certain features that are either bulky, or not needed from the software effectively optimizing the software and speeding up the system.

Gentoo also allows its users to compile and tailor the kernel to the hardware the server runs on(down to the processor), this allows you to get the most power out of your hardware.

Now where does distcc come into play in all of this?, compilation on Gentoo is slow, Especially on machine with limited resources(Few cores and RAM).

You also might not want your machines taking up all resources(They might be needing those resources for something else), This is where distcc comes into play.

distcc allows you to offload the compilation process other computers.
Allowing the main machine to assemble the software but leave the compilation up to more powerful computers.

This might sound exciting but there is one limitation to this:
The distcc server has to run the exact same compiler/toolchain version as the client.
An easy solution for this is to run the distcc server in a Gentoo container.
This allows you to use multiple containers for multiple toolchain versions.

## Setting up the cluster
Requirements
You will require the following resources to setup the cluster:
Tip: These can be either virtual or physical instances.
A server running ubuntu(Version does not matter as long as it runs LXD and LXC)
A container on that server that runs the same compiler version as your Gentoo client
A computer or VM running Gentoo.
(Optional) Some patience and tea(Preferably green)
What I used:
 - For my setup I used the following.
 - Container server: Ubuntu 16.04(Xenial)
 - Gentoo VM(Virtualbox)
 - Compiler version(gcc 4.9.4)

### Setup(Distcc Servers):
We will start by setting up the container server, Each node(distcc server) will have its own distcc container.

Once we have booted into the Ubutnu server we run the following command to install the needed software:
`apt-get install lxc lxd ufw`



After that we will create a new container using the following command.
`lxc-create -t gentoo -n gcc-versionum`

Then we will edit the configuration of that container so it will start up at boot.
To do this we will open the config file using vim,

For this we will first need the container name, you can obtain it by using:
`lxc-ls –fancy`
After that we will run the following to edit the file:
`vim /var/lib/lxc/EXAMPLE/config`

We will add the following line to the file to autostart the container at boot:
```
lxc.start.auto = 1 
```
The file should now look as following:
```
# Template used to create this container: /usr/share/lxc/templates/lxc-gentoo 
# Parameters passed to the template: 
# Template script checksum (SHA-1): 87d562970e5cce4032c07ab8a85d210f71c8a922 
# For additional config options, please look at lxc.container.conf(5) 

# Uncomment the following line to support nesting containers: 
#lxc.include = /usr/share/lxc/config/nesting.conf 
# (Be aware this has security implications) 

lxc.network.type = veth 
lxc.network.link = lxcbr0 
lxc.network.flags = up 
lxc.network.hwaddr = MACADDR 
lxc.rootfs = /var/lib/lxc/EXAMPLE/rootfs 
lxc.rootfs.backend = dir 
### lxc-gentoo template stuff starts here 
# sets container architecture 
# If desired architecture != amd64 or x86, then we leave it unset as 
# LXC does not oficially support anything other than x86 or amd64. 
lxc.arch = amd64 

# set the hostname 
lxc.utsname = EXAMPLE 
lxc.tty = 1 

#container set with private portage tree, no mount here 
lxc.include = /usr/share/lxc/config/gentoo.common.conf
lxc.start.auto = 1
```
Do not forget to start and enable the LXC service on the hostmachine:
`systemctl enable lxc lxd; sytemctl start lxc lxd`

Now it is time to start the gentoo container and give it a static ip.

You can start the container by executing the following command:
`lxc-start -n CONTAINERNAME`

After this we will chroot into it using:
`lxc-attach -n CONTAINERNAME`


Now we will set the ip address of the container to 10.0.1.15
Since we wont have any other containers running on this server we won’t need to specify a specific IP.

We can set the ip by adding/changing opening the following file and changing a few lines:
`nano /etc/conf.d/net`

Then change/add the following lines to the following:
`#config_eth0="dhcp"` < Comment out this line
`config_eth0="10.0.3.15/24"` < Set this variable to the following ip 
`routes_eth0="default via 10.0.3.1"` < Set the gateway to the containers hostmachine ip
`dns_servers_eth0="8.8.8.8"` < Use googles DNS server for DNS resolving

After this is finished exit the container by either typing `exit` or hitting Control+D.

Now we have to restart the container using the following commands:
```
lxc-stop -n CONTAINERNAME
lxc-start -n CONTAINERNAME
```

Now we will attach to the container again, you can reboot the server to see if everything worked by typing `lxc-ls –fancy` again to see if the ip shows up and the container started.

We can attached using:
`lxc-attach -n CONTAINERNAME`


Its time to setup distcc on the container.
First we set the MAKEOPTS in portage to make portage utilize all resources on the server.
We can set this by opening the following file with:
`nano /etc/portage/make.conf`

And then add the following lines to the end of it:
```
MAKEOPTS="-jN" 
EMERGE_DEFAULT_OPTS="-jN"
```

Where N is the amount of cores the system has + 1.
So if the system has 5 processor cores then N must be 6.(5+1=6)

After this we will first install distcc, we can do this by executing the following:
`emerge --ask sys-devel/distcc`

If you are happy with the changes that will be executed then type Yes and hit enter.

After this we will need to edit the main file of distcc to allow localhost to contact the distcc server, lets open that file for editing:
`nano /etc/conf.d/distccd`

Now we will fist need to change the `DISTCCD_OPTS` variable to the following:
```
DISTCCD_OPTS="--port 3632 --log-level notice --log-file /var/log/distccd.log -N 15 --allow 192.168.0.1/24 --allow 10.0.3.0/24 --allow localhost”
```
Now we need to enable and start the service.
```
rc-update add distccd default 
rc-service distccd start 
```
Now its time to setup the firewall dnat forwarding rules.

First exit the container to go back to the hostmachine.
Assuming you have installed UFW we will now open the following file to add the rules(Make sure to add these above the `*filter` block):
    `vim /etc/ufw/before.rules`

Add the following rules above the `*filter` block:
```
*nat 
:PREROUTING ACCEPT [0:0] 
-A PREROUTING -i ens3 -p tcp --dport 3632 -j DNAT --to 10.0.3.15:3632 
-A PREROUTING -i ens3 -p udp --dport 3632 -j DNAT --to 10.0.3.15:3632 
COMMIT
```
Next up we will set ipv4 packet forwarding.
Open sysctl.conf using the following command:
`vim /etc/sysctl.conf`
    
We will need to change/add/uncomment the following line to the file:
`net.ipv4.ip_forward=1`
After this we will run the following commands to ensure that our firewall runs and allows SSH and our client to connect.
```
ufw allow ssh
ufw allow from CLIENT_IP to any
systemctl enable ufw
systemctl start ufw
ufw enable
```

Now all that is left is to reboot the server to make sure everything runs as planned.
### Setup(Gentoo client):

I assume that you already have a Gentoo client.
If not you can find information regarding the installation of gentoo at wiki.gentoo.org

First we install distcc on the client:
`emerge --ask  sys-devel/distcc`

After that we will set the hostfile to include the distcc hosts.
Open the hosts file by typing:
`nano /etc/distcc/hosts`
    
Next up add lines in this format:
`IP,cpp,lzo`
    
(EXAMPLE):
`192.168.0.101,cpp,lzo`

When you have added all your compilation servers, save the file and exit.

Now we need to configure portage to use the distcc servers.
Open the /etc/portage/make.conf file.
`nano /etc/portage/make.conf`
    
And add/change/uncomment the following lines:
```
FEATURES=”distcc distcc-pump`
```
Next up we will set the amount of jobs that Gentoo will create for each compilation.
First we will calculate N by first counting all the cores available from the servers and localhost.

Say our client has 4 cores and we have 2 servers having each 8 cores, We will first count the amount of computers which is 3, next we will count the total amount of cores which is 20. Now we will calculate the amount of jobs we can create.
Computers: 3
Cores: 20
(20+3)*2=46 Jobs

Therefore N is 46.
To prevent our client from taking all the load we will add a maximum load average.

We will set this to 2 because we only want it to use 2 cores for compilation(This also prevents Gentoo to run 46 jobs locally when the distcc servers are not available).
Now we will change/add the following two lines to ensure everything runs smoothly:
```
MAKEOPTS=”-j46 -l2”
EMERGE_DEFAULT_OPTS=”-j46”
```

And that is it.
You now have a distcc cluster assisting your compiling needs!

## Common faults
Of course there are a few things that can be overlooked that might cause issues.
For starters always check if  your client can reach the server on the distcc port.
Also make sure that the distcc port is running and the container has the same compiler version as the client.


Written by Jeroen Mathon – Linux Engineer / Enthusiast.
